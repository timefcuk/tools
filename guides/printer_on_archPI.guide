# CUPS installation and configuration guide

## Overview
This is a guide that helps you to install and configure CUPS on Raspberry Pi that runs Arch Linux

## Manual

0. Install ArchPi on the machine using the official guide
   0.1 Connect Rpi to the internet via shared connection with your laptop, then configure to use your local wifi
1. Generate RSA key for SSH `ssh-keygen -t rsa` and make sure that printer is connected to your RPi
2. Connect to your machine (l:alarm, p:alarm) -> log in as root `su root` (p:root)
   2.1 Change mirror server to de in /etc/pacman.d/mirrorlist
   2.2 Install dependencies: `pacman -Syu vim wget --noconfirm`
3. Create new user and add a password for him `useradd -m -g wheel -s /bin/bash cupsuser && passwd cupsuser`
5. Close the SSH session and log in again as cupsuser using your new password, then log in as root `su root` (p:root)
4. Remove alarm user and change root password `userdel -r alarm && passwd root`
5. SSH configuration 
  1. Change SSH port to the custom one `vim /etc/ssh/sshd_config` (Port 22 -> Port 1337)
  2. Uncomment PermitRootLogin and set it to yes (#PermitRootLogin prohibit-password -> PermitRootLogin yes)
  3. Restart sshd service `systemctl restart sshd`
6. Update the system and install all the dependencies (splix is a Samsung driver) `pacman -Syu cups ghostscript gsfonts samba splix vim avahi --noconfirm` 
7. Start and enable cups, samba and avahi services
   7.1 `wget -O /etc/samba/smb.conf 'https://git.samba.org/samba.git/?p=samba.git;a=blob_plain;f=examples/smb.conf.default;hb=HEAD' && chmod 644 /etc/samba/smb.conf`
   7.2 `systemctl start avahi-daemon org.cups.cupsd.service smb && systemctl enable avahi-daemon org.cups.cupsd.service smb` 
   7.3 Open /etc/samba/smb.conf and allow printers to be browseble, guest ok = yes and public = yes too, you can also redirect logs as per the unit error.
8. Close your SSH session
9. Now you have to create a tunnel that allows you to connect to cups configuration site that runs on raspberry pi. `ssh root@192.168.12.34 -p 1337 -L 12345:localhost:631`
10. While connection is established, open browser and go to (http://localhost:12345)
11. Go to Administration page -> log in (l:root p:root password) -> Check Share printers connected to the system checkbox
12. Go to Find new printers -> choose your printer -> Input all the data and don't forget to check "Share this printer" checkbox -> Choose manufacturer and the model of your printer -> set default properties -> add the printer
13. Install `system-config-printer`, `gtk3-print-backends`, `cups` on your local machine -> `sudo systemctl start avahi-daemon org.cups.cupsd.service && systemctl enable avahi-daemon org.cups.cupsd.service`  printer has to be found and configured automatically on linux machine, otherwise, configure it yourself using the first tool
14. In Windows add printer via http://ip:port/printers/printername (driver has to be installed) 
 

## Troubleshooting

* There can be a problem when pacman couldn't download some packages from the repository returning the following message
> Connection timed out after 10690 milliseconds
I fixed the problem by simply commenting the default server and uncommenting another server (perhaps from Germany) in /etc/pacman.d/mirrorlist

* If you're so tired of beep sound on every wrong click in the terminal, uncomment `set bell-style none` in /etc/inputrc

* If you've changed some cups settings, it will try to restart the service. If some time passed and you still can't get to cups site, restart cups service manually via `systemctl restart org.cups.cupsd.service` 

## TODO
* Add autoreboot in cron
