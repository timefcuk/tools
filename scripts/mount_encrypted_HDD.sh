#!/bin/bash
# This is a script that will help with mounting/umounting of my new encrypted backup HDD

CHOICE=${1:-mount}
NAME=${2:-vault_86}
FS=${3:-ext4}
MOUNTPOINT=${4:-/run/mount/$NAME}
DEVICE=${5:-/dev/sdb1}

case $CHOICE in
  mount)
    echo " Mounting $NAME of $FS from $DEVICE to $MOUNTPOINT"
    mkdir $MOUNTPOINT
    chmod 777 $MOUNTPOINT
    cryptsetup open $DEVICE $NAME 
    mount -t ext4 /dev/mapper/$NAME $MOUNTPOINT
    ;;
  umount)
    echo " Umounting $NAME of $FS from $DEVICE on $MOUNTPOINT"
    umount /dev/mapper/$NAME
    cryptsetup close $NAME
    rm -r $MOUNTPOINT
    ;;
  *)
    echo "Wrong choice. Pls check the script"
esac
