#!/usr/bin/python
# this is a simple script for measuring the size of a file in the web

import os, sys, urllib.request

# path for downloaded file
path='/tmp/pygetsize.file'

# arguments from terminal
args = sys.argv

try:
    if (args[1] == None) or (args[1] == ""):
    # displaying default message
        print("ENTER URL\n\
                Example: ./pygetsize.py http://ddg.gg\n\
                For help type --help")
        sys.exit(1)

    elif args[1] == "--help":
    # displaying help message
        print('pygetsize v0.1\n\
                This utilite returns the size of a file that is located at given URL\n\n\
                Example:\n\
                ./pygetsize:.py http://ddg.gg\n\
                To get this message type --help')
        sys.exit(1)

    else:
    # downloading the file
        retrieve = urllib.request.urlretrieve(url=args[1], filename=path)
    # printing size of the file
        print(os.stat(path).st_size)
    # deleting the file
        os.remove(path)
        sys.exit(0)
except IndexError:
# displaying default message
    print("ENTER URL\n\
            Example: ./pygets.py http://ddg.gg\n\
            For help type --help")
    sys.exit(1)

