#!/bin/sh

echo "UPDATING" &>> /home/centos/log
yum install vim -y &>> /home/centos/log
echo "UMOUNTING" &>> /home/centos/log
umount /dev/xvdz &>> /home/centos/log
echo "UMOUNTED -> FORMATTING" &>> /home/centos/log
#parted --script /dev/xvdz \ mklabel msdos \ mkpart primary ext4 0% 100% \ align-check min 1
mkfs.ext4 -q /dev/xvdz &>> /home/centos/log
echo "FORMATTED -> MOUNTING" &>> /home/centos/log
mount /dev/xvdz /mnt &>> /home/centos/log
echo "DONE" &>> /home/centos/log

