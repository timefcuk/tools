#!/bin/bash

# Parent AMI
AMI="ami-d2c924b2"
# Name for a key
KEYNAME="apopryginKEY"
# Name for security group
SGNAME="apopryginSecurityGroup"
# Description for snapshot
SNAPDESCR="apoprygin_launch.sh_script_snapshot"
# Name for AMI
AMINAME="apoprygin_launch.sh_script_ami"

echo 	"\
This script: \n\n\
If you choose option \"launch\":\n\
1. Opens aws configuration \n\
2. Creates key pair \n\
3. Creates security group \n\
4. Launches CentOS 7 instance with ext4 formatted ephemeral volume and updates the system \n\
5. Terminates the instance, creates a snapshot of it \n\
6. Registers a new image with EBS volume restored from the snapshot \n\
7. Launches a new instance from the image \n\n\
\
If you choose option \"clean\":\n\
1. Terminate the new instance\n\
2. Deletes the image\n\
3. Deletes the snapshot\n\
4. Deletes the key\n\
5. Deletes the security group\n\n\
\
Logs are stored in ~/log file on every instance 
Local logs are stored in /tmp/log.aws \n\n\
\
Choose the option? (launch/clean)\
"

read answer

if [ $answer = "launch" ] 
then
	echo $(date -R)" | Launching configuration utility" | tee /tmp/log.aws
	aws configure

	
	echo $(date -R)" | Creating new key pair\
$(aws ec2 create-key-pair --key-name $KEYNAME --query 'KeyMaterial' --output text > /tmp/$KEYNAME.pem)" | tee -a /tmp/log.aws
	chmod 600 /tmp/$KEYNAME.pem
	echo $(date -R)" | Key $KEYNAME with fingerprint \
$(aws ec2 describe-key-pairs --filters "Name=key-name,Values=$KEYNAME" | jq '.[] | .[] | .KeyFingerprint') was created" | tee -a /tmp/log.aws

	echo $(date -R)" | Creating new security group" | tee -a /tmp/log.aws
	aws ec2 create-security-group --group-name $SGNAME --description "Security group created by apoprygin launch.sh script" >> /tmp/log.aws
	SGID=$(aws ec2 describe-security-groups --filters "Name=group-name,Values=$SGNAME" | jq '.SecurityGroups | .[] | .GroupId' | tr -d \")
	echo $(date -R)" | New group ID: $SGID" | tee -a /tmp/log.aws
	echo $(date -R)" | Opening 22 port for any source address in $SGID" | tee -a /tmp/log.aws
	aws ec2 authorize-security-group-ingress --group-id $SGID --protocol tcp --port 22 --cidr 0.0.0.0/0 >> /tmp/log.aws
	
	echo $(date -R)" | Launching new instance from $AMI" | tee -a /tmp/log.aws
	IID1=$(aws ec2 run-instances --image-id $AMI --count 1 --instance-type m3.medium --key-name $KEYNAME --security-group-ids $SGID --block-device-mappings https://bitbucket.org/timefcuk/tools/raw/c86df0618519357ecfb1224fdc2c9c25827b5266/scripts/aws/mapping.json --user-data https://bitbucket.org/timefcuk/tools/raw/c86df0618519357ecfb1224fdc2c9c25827b5266/scripts/aws/partitioning.sh | jq '.Instances | .[0].InstanceId' | tr -d \")
	echo $(date -R)" | New instance ID is $IID1" | tee -a /tmp/log.aws
	IP1=$(aws ec2 describe-instances  --filters "Name=instance-id,Values=$IID1" | jq '.Reservations | .[] | .Instances | .[].PublicIpAddress' | tr -d \")
	
	VOLID=$(aws ec2 describe-instances  --filters "Name=instance-id,Values=$IID1" | jq '.Reservations | .[] | .Instances | .[].BlockDeviceMappings| .[0].Ebs.VolumeId'| tr -d \")

	echo $(date -R)" | New instance IP is $IP1; VolumeID is $VOLID" | tee -a /tmp/log.aws
	echo $(date -R)" | Stopping the $IID1 instance. Creating a snapshot (wait 50 seconds for instance to stop)" | tee -a /tmp/log.aws
	#ec2 create-snapshot --volume-id $VOLID --description "A snapshot created by apoprygin launch.sh script " >> /tmp/log.aws
	aws ec2 terminate-instances --instance-ids $IID1 >> /tmp/log.aws
	sleep 50
	SNAPID=$(aws ec2 create-snapshot --volume-id vol-078dfcfddcda55e32 --description $SNAPDESCR | jq '.SnapshotId' | tr -d \" )	
	echo $(date -R)" | New snapshot ID is $SNAPID" | tee -a /tmp/log.aws

	echo $(date -R)" | Creating AMI from snapshot" | tee -a /tmp/log.aws
	AMIID=$(aws ec2 register-image --name $AMINAME --block-device-mappings "[{\"DeviceName\": \"/dev/xvda\",\"Ebs\":{\"SnapshotId\":\"$SNAPID\"}}]"  --root-device-name /dev/xvda --architecture x86_64 --virtualization-type hvm | jq '.ImageId' | tr -d \" )
	sleep 10	
	echo $(date -R)" | AMI ID is $AMIID" | tee -a /tmp/log.aws

	echo $(date -R)" | Running new instance on created AMI" | tee -a /tmp/log.aws
	IID2=$(aws ec2 run-instances --image-id $AMIID --count 1 --instance-type m3.medium --key-name $KEYNAME --security-group-ids $SGID --block-device-mappings https://bitbucket.org/timefcuk/tools/raw/c86df0618519357ecfb1224fdc2c9c25827b5266/scripts/aws/mapping.json | jq '.Instances | .[0].InstanceId' | tr -d \")
	echo $(date -R)" | New instance id is $IID2 " | tee -a /tmp/log.aws
	IP2=$(aws ec2 describe-instances  --filters "Name=instance-id,Values=$IID2" | jq '.Reservations | .[] | .Instances | .[].PublicIpAddress' | tr -d \")
	echo $(date -R)" | You can connect now to new instance by ssh: \"ssh -i /tmp/$KEYNAME.pem centos@$IP2\" " | tee -a /tmp/log.aws



	exit 0;
elif [ $answer = "clean" ]
then

	IID=$(aws ec2 describe-instances  --filters "Name=key-name,Values=$KEYNAME" "Name=instance-state-code,Values=16" | jq '.Reservations | .[] | .Instances | .[] | .InstanceId'| tr -d \")
	echo $(date -R)" | Terminating instance $IID"| tee -a /tmp/log.aws

	aws ec2 terminate-instances --instance-ids $IID
	echo $(date -R)" | Everything is OK - script will wait 50 seconds" | tee -a /tmp/log.aws
	
	sleep 50

	AMIID=$(aws ec2 describe-images --owner self --filter "Name=name,Values=$AMINAME" | jq '.Images | .[0].ImageId' | tr -d \")
	echo $(date -R)" | Deleting image $AMIID"| tee -a /tmp/log.aws

	aws ec2 deregister-image --image-id $AMIID
	sleep 5

	SNAPID=$(aws ec2 describe-snapshots --filter "Name=description,Values=$SNAPDESCR" | jq '.Snapshots | .[].SnapshotId' | tr -d \" )
	echo $(date -R)" | Deleting snapshot $SNAPID"| tee -a /tmp/log.aws

	aws ec2 delete-snapshot --snapshot-id $SNAPID
	
	echo $(date -R)" | Deleting key $KEYNAME"| tee -a /tmp/log.aws

	aws ec2 delete-key-pair --key-name $KEYNAME
	rm /tmp/$KEYNAME.pem

	echo $(date -R)" | Deleting security group rule"| tee -a /tmp/log.aws

	SGID=$(aws ec2 describe-security-groups --filters "Name=group-name,Values=$SGNAME" | jq '.SecurityGroups | .[] | .GroupId' | tr -d \" )
	echo $(date -R)" | Security group code: $SGID "| tee -a /tmp/log.aws

 	aws ec2 revoke-security-group-ingress --group-id $SGID --protocol tcp --port 22 --cidr 0.0.0.0/0
	sleep 15
	echo $(date -R)" | Deleting secutiry group itself"| tee -a /tmp/log.aws

	aws ec2 delete-security-group --group-id $SGID

	exit 0;
else 
	echo "Wrong answer supplied - restart the script"
	exit 1;
fi


