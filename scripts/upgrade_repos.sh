#!/bin/bash
# script, that upgrades pacman's repositories for fast and reliable download sources
# without backup
echo "updating mirrorlist.."
sudo reflector --verbose -l 10 --sort rate --save /etc/pacman.d/mirrorlist
echo "updated. check /etc/pacman.d/mirrorlist"
