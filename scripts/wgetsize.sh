#!/bin/bash
#!/usr/bin/sh
# this is a simple script for measuring the size of a file stored on server via URL  
filename="wgetsize.file"
logname="wgetsize.log"
if [[ $1 == "--help" ]]
then
	echo "wgetsize v0.2"
	echo -e "This utilite returns size of a file that is located at given URL \n"
	echo "Example:"
	echo " ./wgetsize.sh https://tools.ietf.org/rfc/rfc2616.txt"
	echo -e "\nTo get this message, type:"
	echo " ./wgetsize.sh --help"
	exit 1;
elif [[ $1 == "" ]]
then
	echo -e "Enter URL\n Example: ./wgetsize.sh https://tools.ietf.org/rfc/rfc2616.txt\n"
	echo "Enter ./wgetsize.sh --help for help message"
	exit 1;
else
	wget -O /tmp/$filename $1 -o /tmp/$logname
	du -b /tmp/$filename | cut -f1
	rm /tmp/$logname /tmp/$filename
	exit 0;
fi
exit 1;
