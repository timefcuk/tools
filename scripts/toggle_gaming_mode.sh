#!/bin/bash
sudo rmmod intel_powerclamp &
level=$(grep "level:" /proc/acpi/ibm/fan | grep "auto")
if [ -z "$level" ]
then
echo level auto | sudo tee /proc/acpi/ibm/fan
else
echo level disengaged | sudo tee /proc/acpi/ibm/fan
fi

