#!/usr/bin/python

import random
import sys
import subprocess
import os

class GenerateColor:
	
	def __init__(self):
		try:
			path = sys.argv[1]
		except:
			path = None
		if path==None:
			print(self.RandomColor())
			exit()
		else:
			path="/home/apoprygin/.config/i3/colors.txt"
			print(self.SafeColor(path))
			exit()

	def SafeColor(self,path):
		if os.path.exists(path):
			with open(path) as f:
				content = f.readlines()
			content = [x.strip() for x in content] 
			return random.choice(content)
		else:
			subprocess.call("/home/apoprygin/.config/i3/./colorcrawler.sh")
			A = GenerateColor()
			return A.SafeColor(path)
		

	def RandomColor(self):
		vocabulary = ["A","B","C","D","E","F","0","1","2","3","4","5","6","7","8","9"]
		code = ""

		for i in range (0,6):
			code+=vocabulary[random.randint(0,len(vocabulary)-1)]
		return code
	
A = GenerateColor()
